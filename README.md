# README

The classic hello_app from railstutorial.org/book/

Only diffs are using rails 5.1.6 instead of 5.1.4 and made the switch
from sqlite3 to postgreSQL in development. HOPE these changes don't
bite me later... :) -djb

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
