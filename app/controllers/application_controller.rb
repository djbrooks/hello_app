class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "hello, world! -OR- ¡Hola, mundo!"
  end

  def goodbye
    render html: "Hello Sandra and goodbye, world! -OR- ¡Adios, mundo!"
  end

end
